#!/usr/bin/env python
# coding: utf-8

# Import Packages

## Standard Packages
import re

## Installed Packages
import pytest

## Custom Packages
import fuzzyjoin.data_cleaning.data_cleaning as dc


test_remove_punctuation_params = [
    ('Multimedia Databases.', 'Multimedia Databases '),
    ('Multimedia Databases ¾', 'Multimedia Databases ¾')
]

@pytest.mark.parametrize(
    'test_data, expected_output',
    test_remove_punctuation_params
)
def test_remove_punctuation(test_data, expected_output):
    assert dc.remove_punctuation(test_data) == expected_output



test_normalize_case_params = [
    ('Multimedia Databases ', 'multimedia databases ')
]

@pytest.mark.parametrize(
    'test_data, expected_output',
    test_normalize_case_params
)
def test_normalize_case(test_data, expected_output):
    assert dc.normalize_case(test_data) == expected_output

test_trim_params = [
    ('Multimedia Databases ', 'Multimedia Databases'),
    ('Multimedia   Databases ', 'Multimedia Databases'),
    (' Multimedia Databases ', 'Multimedia Databases')
]

@pytest.mark.parametrize(
    'test_data, expected_output',
    test_trim_params
)
def test_trim(test_data, expected_output):
    assert dc.trim(test_data) == expected_output

test_string_cleaning_params = [
    ('Multimedia Databases.', 'multimedia databases')]

@pytest.mark.parametrize(
    'test_data, expected_output',
    test_string_cleaning_params
)
def test_string_cleaning(test_data, expected_output):
    assert dc.string_cleaning(test_data) == expected_output

