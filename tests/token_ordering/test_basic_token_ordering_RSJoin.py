#!/usr/bin/env python
# coding: utf-8

# Import Packages
## Standard Packages
from functools import reduce as reduce_ft

## Installed Packages
from pyspark import SparkContext
from pyspark.sql import SparkSession, SQLContext, Window
import pyspark.sql.functions as F
import pytest

## Custom Packages
import fuzzyjoin.tokenization.token_process as tp
import fuzzyjoin.data_cleaning.data_cleaning as dc
import fuzzyjoin.similarity_scores.similarity_scores as sim
import fuzzyjoin.token_ordering.basic_token_ordering_RS_join as bto
import fuzzyjoin.helpers as H


# Load expected output of token ordering
# NOTE: This output was extracted from original fuzzyjoin repo
test_token_ordering = []
with open('data/pub-small.expected/tokens-000/expected.txt', 'r') as f:
    for line in f:
        test_token_ordering.append(line.strip())


def test_bto_rs_join(df_R_filepath='data/pub-small/raw.dblp-000/part-00000', 
                     join_columns_R=['publication_title','authors'], sep_R=':', 
                     schema_R='dblp_id STRING, publication_title STRING, authors STRING, other_info STRING',
                     record_data_R="2,3",
                     df_S_filepath='data/pub-small/raw.csx-000/part-00000', 
                     join_columns_S=['publication_title','authors'], sep_S=':', 
                     schema_S='csx_id STRING, publication_title STRING, authors STRING, other_info STRING',
                     record_data_S="2,3"):
    # Start Spark Session
    sc = SparkContext()
    spark = SparkSession.builder.getOrCreate()
    
    # NOTE: These datasets were extracted from original fuzzyjoin repo
    
    # Load DBLP Dataset
    df_R = spark.read.csv(path=df_R_filepath, sep=sep_R, schema=schema_R)
    n_R = df_R.count()
    
    # Load CITSEERX Dataset
    df_S = spark.read.csv(path=df_S_filepath, sep=sep_S, schema=schema_S)
    n_S = df_S.count()
    
    # Add record ID
    df_R = H.create_record_id(df=df_R)
    df_S = H.create_record_id(df=df_S)
    
    # Setup join attributes
    df_R = H.create_join_attribute(df=df_R, join_columns=join_columns_R, record_data=record_data_R)
        
    df_S = H.create_join_attribute(df=df_S, join_columns=join_columns_S, record_data=record_data_S)
    
    ## Basic Token Ordering    
    token_ordering = bto.bto_RS_Join(df_R=df_R, n_R=n_R, df_S=df_S, n_S=n_S, 
                                 join_R='join_attribute', join_S='join_attribute')
    token_ordering_list = token_ordering.collect()
    
    assert set(test_token_ordering) == set(token_ordering_list)
    
    # Close the Spark Context
    sc.stop()