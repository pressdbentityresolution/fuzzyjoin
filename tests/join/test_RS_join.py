#!/usr/bin/env python
# coding: utf-8

# Import Packages
## Standard Packages
from operator import add
from functools import reduce as reduce_ft

## Installed Packages
from pyspark import SparkContext
from pyspark.sql import SparkSession, SQLContext, Window
import pyspark.sql.functions as F
import pandas as pd
import pytest

## Custom Packages
import fuzzyjoin.RS_join as RS_join


# Load expected output for record join
# NOTE: This output was extracted from original fuzzyjoin repo
test_df_similar_pairs = pd.read_csv("data/pub-small.expected/recordpairs-000/expected.txt", sep=":",
                                   names=["RID1","dblp_id", "dblp_publication_title",
                                          "dblp_authors","dblp_other_info","csx_id","csx_publication_title",
                                          "csx_authors","csx_other_info"])

test_df_similar_pairs[["dblp_other_info","JaccardSimilarity","RID2"]] = test_df_similar_pairs["dblp_other_info"].str.split(";", expand=True)

test_df_similar_pairs = test_df_similar_pairs.sort_values(by=["dblp_id", "csx_id"]).reset_index(drop=True)

def test_rs_join(df_R_filepath='data/pub-small/raw.dblp-000/part-00000', 
                     join_columns_R=['publication_title','authors'], sep_R=':', 
                     schema_R='dblp_id STRING, publication_title STRING, authors STRING, other_info STRING',
                     record_data_R="2,3", record_id_R="dblp_id",
                     sim_threshold=0.5, 
                     df_S_filepath='data/pub-small/raw.csx-000/part-00000', 
                     join_columns_S=['publication_title','authors'], sep_S=':', 
                     schema_S='csx_id STRING, publication_title STRING, authors STRING, other_info STRING',
                     record_data_S="2,3", record_id_S="csx_id"):


    # Start Spark Session
    sc = SparkContext()
    spark = SparkSession.builder.getOrCreate()

    df_similar_pairs = RS_join(spark=spark, df_R_filepath=df_R_filepath, 
                               join_columns_R=join_columns_R, sep_R=sep_R, schema_R=schema_R, 
                               record_data_R=record_data_R, record_id_R=record_id_R,
                               sim_threshold=sim_threshold,
                               df_S_filepath=df_S_filepath, join_columns_S=join_columns_S, 
                               sep_S=sep_S, schema_S=schema_S, record_data_S=record_data_S, record_id_S=record_id_S)

    assert df_similar_pairs is not None

    # Test Output Pair Matching
    df_similar_pairs = df_similar_pairs.toPandas()
    df_similar_pairs.columns = ["dblp_id", "csx_id","JaccardSimilarity"]

    df_similar_pairs = df_similar_pairs.sort_values(by=["dblp_id", "csx_id"]).reset_index(drop=True)
    
    assert df_similar_pairs[["dblp_id", "csx_id"]].equals(test_df_similar_pairs[["dblp_id", "csx_id"]])
    
    temp_f = lambda x: round(float(x), 6)
    assert list(df_similar_pairs["JaccardSimilarity"].apply(temp_f)) == list(test_df_similar_pairs["JaccardSimilarity"].apply(temp_f))
    
    # Close the Spark Context
    sc.stop()

