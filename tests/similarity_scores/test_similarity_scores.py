#!/usr/bin/env python
# coding: utf-8

# Import Packages


## Installed Packages
import pytest

## Custom Packages
import fuzzyjoin.similarity_scores.similarity_scores as sim

test_jaccard_index_params = [
    (["I", "will", "call", "back"], ["I", "will", "call", "soon"], 0.6)
]


@pytest.mark.parametrize(
    'test_A, test_B, expected_output',
    test_jaccard_index_params
)
def test_jaccard_index(test_A, test_B, expected_output):
    assert sim.jaccard_index(test_A, test_B) == expected_output

test_weight_jaccard_params = [
    ("I will call back", "I will call soon", 
     {"i_1":5, "will_1":10, "call_1":5, "back_1":2, "soon_1":10}, 
     0.6310892149869802) 
]
@pytest.mark.parametrize(
    'test_A, test_B, test_token_counts, expected_output',
    test_weight_jaccard_params
)
def test_jaccard_index(test_A, test_B, test_token_counts, expected_output):
    assert round(sim.weighted_jaccard_index(test_A, test_B, test_token_counts), 7) == round(expected_output, 7)


test_overlap_similarity_params = [
    (["I", "will", "call", "back"], ["I", "will", "call", "soon"], 3)
]

@pytest.mark.parametrize(
    'test_A, test_B, expected_output',
    test_overlap_similarity_params
)
def test_overlap_similarity(test_A, test_B, expected_output):
    assert sim.overlap_similarity(test_A, test_B) == expected_output


test_hamming_distance_params = [
    (["B", "C", "D", "E", "F"], ["A", "B", "C", "D", "E"], 2)
]
@pytest.mark.parametrize(
    'test_A, test_B, expected_output',
    test_hamming_distance_params
)
def test_hamming_distance(test_A, test_B, expected_output):
    assert sim.hamming_distance(test_A, test_B) == expected_output


test_lev_distance_params = [
    ("kitten", "sitting", 3),
    ("kitten", "kitten", 0),
    ("flaw", "lawn", 2)
    
]
@pytest.mark.parametrize(
    'test_s, test_t, expected_output',
    test_lev_distance_params
)
def test_levenshtein_distance(test_s, test_t, expected_output):
    assert sim.levenshtein_distance(test_s, test_t) == expected_output

test_haversine_distance_params = [
    (30, 30, 30, 30, 0)
]
@pytest.mark.parametrize(
    'test_lon_a, test_lat_a, test_lon_b, test_lat_b, expected_output',
    test_haversine_distance_params
)
def test_haversine_distance(test_lon_a, test_lat_a, test_lon_b, test_lat_b, expected_output):
    assert sim.haversine_distance(test_lon_a, test_lat_a, test_lon_b, test_lat_b) == expected_output
