#!/usr/bin/env python
# coding: utf-8

# Import Packages
## Standard Packages
from operator import add
from functools import reduce as reduce_ft

## Installed Packages
from pyspark import SparkContext
from pyspark.sql import SparkSession, SQLContext, Window
import pyspark.sql.functions as F
import pandas as pd
import pytest

## Custom Packages
import fuzzyjoin.tokenization.token_process as tp
import fuzzyjoin.data_cleaning.data_cleaning as dc
import fuzzyjoin.similarity_scores.similarity_scores as sim
import fuzzyjoin.token_ordering.basic_token_ordering_RS_join as bto
import fuzzyjoin.helpers as H
import fuzzyjoin.kernel.basic_kernel_RSJoin as bk


# Load expected output for record join
# NOTE: This output was extracted from original fuzzyjoin repo
test_df_similar_pairs = pd.read_csv("data/pub-small.expected/recordpairs-000/expected.txt", sep=":",
                                   names=["RID1","dblp_id", "dblp_publication_title",
                                          "dblp_authors","dblp_other_info","csx_id","csx_publication_title",
                                          "csx_authors","csx_other_info"])

test_df_similar_pairs[["dblp_other_info","JaccardSimilarity","RID2"]] = test_df_similar_pairs["dblp_other_info"].str.split(";", expand=True)

test_df_similar_pairs = test_df_similar_pairs.sort_values(by=["dblp_id", "csx_id"]).reset_index(drop=True)


def test_basic_kernel_rs_join(df_R_filepath='data/pub-small/raw.dblp-000/part-00000', 
                     join_columns_R=['publication_title','authors'], sep_R=':', 
                     schema_R='dblp_id STRING, publication_title STRING, authors STRING, other_info STRING',
                     record_data_R="2,3",
                     sim_threshold=0.5, 
                     df_S_filepath='data/pub-small/raw.csx-000/part-00000', 
                     join_columns_S=['publication_title','authors'], sep_S=':', 
                     schema_S='csx_id STRING, publication_title STRING, authors STRING, other_info STRING',
                     record_data_S="2,3"):
    # Start Spark Session
    sc = SparkContext()
    spark = SparkSession.builder.getOrCreate()
    
    # NOTE: These datasets were extracted from original fuzzyjoin repo
    
    # Load DBLP Dataset
    df_R = spark.read.csv(path=df_R_filepath, sep=sep_R, schema=schema_R)
    n_R = df_R.count()
    
    # Load CITSEERX Dataset
    df_S = spark.read.csv(path=df_S_filepath, sep=sep_S, schema=schema_S)
    n_S = df_S.count()
    
    # Add record ID
    df_R = H.create_record_id(df=df_R)
    df_S = H.create_record_id(df=df_S)
    
    # Setup join attributes
    df_R = H.create_join_attribute(df=df_R, join_columns=join_columns_R, record_data=record_data_R)
        
    df_S = H.create_join_attribute(df=df_S, join_columns=join_columns_S, record_data=record_data_S)
    
    ## Basic Token Ordering    
    token_ordering = bto.bto_RS_Join(df_R=df_R, n_R=n_R, df_S=df_S, n_S=n_S, 
                                 join_R='join_attribute', join_S='join_attribute')
    token_ordering_list = token_ordering.collect()
    
    ## RID-Pair Generation - Basic Kernel
    
    
    similar_pairs = bk.basic_kernel_RSJoin(df_R=df_R, RID_name_R="RID", join_attribute_name_R="join_attribute", 
                            df_S=df_S, RID_name_S="RID", join_attribute_name_S="join_attribute", 
                            token_ordering=token_ordering_list, sim_threshold=sim_threshold)
    
    
    assert similar_pairs.isEmpty() is False
    
    # Record Join
    df_similar_pairs = spark.createDataFrame(similar_pairs, ["RID1", "RID2", "JaccardSimilarity"])
    df_similar_pairs = df_similar_pairs.join(df_R, df_similar_pairs.RID1 == df_R.RID).join(df_S, df_similar_pairs.RID2 == df_S.RID)
    df_similar_pairs = df_similar_pairs.toPandas()
    df_similar_pairs.columns = ["RID1","RID2","JaccardSimilarity","dblp_id","dblp_publication_title",
                            "dblp_authors","dblp_other_info","RID1_copy", "dblp_join_attribute",
                            "csx_id","csx_publication_title",
                            "csx_authors","csx_other_info","RID2_copy", "csx_join_attribute"]

    df_similar_pairs = df_similar_pairs.sort_values(by=["dblp_id", "csx_id"]).reset_index(drop=True)
    
    assert df_similar_pairs[["dblp_id", "csx_id"]].equals(test_df_similar_pairs[["dblp_id", "csx_id"]])
    
    temp_f = lambda x: round(float(x), 6)
    assert list(df_similar_pairs["JaccardSimilarity"].apply(temp_f)) == list(test_df_similar_pairs["JaccardSimilarity"].apply(temp_f))
    
    # Close the Spark Context
    sc.stop()
