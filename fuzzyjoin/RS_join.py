#!/usr/bin/env python
# coding: utf-8

# Import Packages
## Standard Packages
from operator import add
from functools import reduce as reduce_ft

## Installed Packages
from pyspark import SparkContext
from pyspark.sql import SparkSession, SQLContext, Window
import pyspark.sql.functions as F
import pandas as pd

## Custom Packages
import fuzzyjoin.tokenization.token_process as tp
import fuzzyjoin.data_cleaning.data_cleaning as dc
import fuzzyjoin.similarity_scores.similarity_scores as sim
import fuzzyjoin.token_ordering.basic_token_ordering_RS_join as bto
import fuzzyjoin.helpers as H
import fuzzyjoin.kernel.basic_kernel_RSJoin as bk

def RS_join(spark, df_R_filepath, join_columns_R, sep_R, schema_R, record_data_R, record_id_R,
            sim_threshold,
            df_S_filepath, join_columns_S, sep_S, schema_S, record_data_S, record_id_S, **kwargs):
    '''
    PySpark-based implementation of R-S set-similarity join
           Parameters:
                   df_R_filepath (str): Filepath to input dataset R
                   join_columns_R (list): List of column names in R to use for set-similarity join
                   sep_R (str): File delimiter for R
                   schema_R (str): SQL-like schema for R
                   record_data_R (str): Comma-separated string of column indices in R to use for set-similarity join (1-based)
                   record_id_R (str): Name of data field in R to use as record identifier
                   sim_threshold (float): Similarity threshold for Jaccard index metric
                   df_S_filepath (str): Filepath to input dataset S
                   join_columns_S (list): List of column names in S to use for set-similarity join
                   sep_S (str): File delimiter for S
                   schema_S (str): SQL-like schema for S
                   record_data_S (str): Comma-separated string of column indices in S to use for set-similarity join (1-based)
                   record_id_S (str): Name of data field in S to use as record identifier
                   **kwargs: Additional arguments for spark.read.csv
           
           Returns:
                   df (pyspark.sql.DataFrame): Dataset with ids and similarity scores if there are matches, None otherwise
    '''
    # TODO: Create separate functions for loading each dataset, 
    # such that dataset specific arguments can be more easily specified

    # Load Dataset R
    df_R = spark.read.csv(path=df_R_filepath, sep=sep_R, schema=schema_R)
    n_R = df_R.count()
    
    # Load Dataset S
    df_S = spark.read.csv(path=df_S_filepath, sep=sep_S, schema=schema_S)
    n_S = df_S.count()
    
    # Add record ID
    df_R = H.create_record_id(df=df_R)
    df_S = H.create_record_id(df=df_S)
    
    # Setup join attributes
    df_R = H.create_join_attribute(df=df_R, join_columns=join_columns_R, record_data=record_data_R)
        
    df_S = H.create_join_attribute(df=df_S, join_columns=join_columns_S, record_data=record_data_S)
    
    ## Basic Token Ordering    
    token_ordering = bto.bto_RS_Join(df_R=df_R, n_R=n_R, df_S=df_S, n_S=n_S, 
                                 join_R='join_attribute', join_S='join_attribute')
    token_ordering_list = token_ordering.collect()
    
    ## RID-Pair Generation - Basic Kernel
    if record_id_R in [None, [], ""]:
        record_id_R = "RID"
    if record_id_S in [None, [], ""]:
        record_id_S = "RID"
    similar_pairs = bk.basic_kernel_RSJoin(df_R=df_R, RID_name_R=record_id_R, join_attribute_name_R="join_attribute", 
                            df_S=df_S, RID_name_S=record_id_S, join_attribute_name_S="join_attribute", 
                            token_ordering=token_ordering_list, sim_threshold=sim_threshold)
    
    
    # Save Output
    if not similar_pairs.isEmpty():
        df_similar_pairs = spark.createDataFrame(similar_pairs, ["RID1", "RID2", "JaccardSimilarity"])
        return df_similar_pairs
    return None
