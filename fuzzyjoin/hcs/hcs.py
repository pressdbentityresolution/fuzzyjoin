#!/usr/bin/env python
# coding: utf-8

# Import Packages
import networkx as nx


# Fork of https://github.com/53RT/Highly-Connected-Subgraphs-Clustering-HCS
# Python implementation of Hartuv, Erez, and Ron Shamir. "A clustering algorithm based on graph connectivity." Information processing letters 76.4-6 (2000): 175-181.
def highly_connected(G, E):
    """Checks if the graph G is highly connected
    Highly connected means, that splitting the graph G into subgraphs needs more than 0.5*|V| edge deletions
    This definition can be found in Section 2 of the publication.
    :param G: Graph G
    :param E: Edges needed for splitting G
    :return: True if G is highly connected, otherwise False
    """

    return len(E) > len(G.nodes) / 2


def remove_edges(G, E):
    """Removes all edges E from G
    Iterates over all edges in E and removes them from G
    :param G: Graph to remove edges from
    :param E: One or multiple Edges
    :return: Graph with edges removed
    """

    for edge in E:
        G.remove_edge(*edge)
    return G


def HCS(G):
    """Basic HCS Algorithm
    cluster labels, removed edges are stored in global variables
    :param G: Input graph
    :return: Either the input Graph if it is highly connected, otherwise a Graph composed of
    Subgraphs that build clusters
    """
    # NOTE: Minimum edge cut appears to depend on order of edges added to graph
    E = nx.algorithms.connectivity.cuts.minimum_edge_cut(G)
    
    if E == G.edges(): # Base case - Minimum cut produces singletons
        return G

    if not highly_connected(G, E):
        G = remove_edges(G, E)
        sub_graphs = [G.subgraph(c).copy() for c in nx.connected_components(G)]

        if len(sub_graphs) == 2:
            H = HCS(sub_graphs[0])
            _H = HCS(sub_graphs[1])

            G = nx.compose(H, _H)

    return G

def HCS_per_cc(G):
    '''Wrapper that calculates Highly Connected Subgraphs per connected component'''
    if nx.is_connected(G):
        G_HCS = HCS(G)
    else:
        new_graphs = []
        for g in nx.connected_components(G):
            g = nx.Graph(G.subgraph(g))
            g_HCS = HCS(g)
            new_graphs.append(g_HCS)
        G_HCS = nx.algorithms.operators.all.compose_all(new_graphs)
    return G_HCS

def labelled_HCS_per_cc(G):
    """
    Runs basic HCS and returns Cluster Labels
    :param G: Input graph
    :return: List of cluster assignments for the single vertices
    """

    _G = HCS_per_cc(G)

    sub_graphs = (G.subgraph(c).copy() for c in nx.connected_components(_G))


    labels = {}

    for _class, _cluster in enumerate(sub_graphs, 1):
        nodes = list(_cluster.nodes)
        for node in nodes:
            labels[node] = _class

    return labels