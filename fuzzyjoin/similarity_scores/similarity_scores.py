#!/usr/bin/env python
# coding: utf-8

# Import Packages

## Standard Packages
import math

## Installed Packages
import numpy as np

## Custom Packages
import fuzzyjoin.tokenization.token_process as tp
import fuzzyjoin.data_cleaning.data_cleaning as dc

def jaccard_index(A, B):
    '''
    Calculates Jaccard similarity of A and B
           Parameters:
                   A (list): Input string A, as list of tokens
                   B (list): Input string B, as list of tokens
           
           Returns:
                   (float): Jaccard similarity measure
    '''
    A = set(A)
    B = set(B)
    return len(A & B)/len(A | B)

def weighted_jaccard_index(A, B, token_counts):
    '''
    Calculates Jaccard similarity of A and B, weighted by log frequencies of tokens
           Parameters:
                   A (str): Input string A
                   B (str): Input string B
                   token_counts (dict): Mapping of tokens to their raw frequencies
           
           Returns:
                   (float): Jaccard similarity measure
    '''
    all_tokens = token_counts.keys()
    
    A = tp.tokenization(dc.string_cleaning(A))
    A = [x for x in A if x in all_tokens]
    
    B = tp.tokenization(dc.string_cleaning(B))
    B = [x for x in B if x in all_tokens]
    
    A_and_B = list(set(A) & set(B))
    A_or_B = list(set(A) | set(B))
    
    A_and_B_weighted = 0
    for token in A_and_B:
        A_and_B_weighted += math.log(token_counts[token] + 1)
    
    A_or_B_weighted = 0
    for token in A_or_B:
        A_or_B_weighted += math.log(token_counts[token] + 1)
    
    if A_or_B_weighted == 0:
        return None
    
    return A_and_B_weighted/A_or_B_weighted

def overlap_similarity(A, B):
    '''
    Calculates Overlap similarity of A and B
           Parameters:
                   A (list): Input string A, as list of tokens
                   B (list): Input string B, as list of tokens
           
           Returns:
                   (int): Overlap similarity measure
    '''
    A = set(A)
    B = set(B)
    return len(A & B)

def hamming_distance(A, B):
    '''
    Calculates Hamming distance between A and B
           Parameters:
                   A (list): Input string A, as list of tokens
                   B (list): Input string B, as list of tokens
           
           Returns:
                   (int): Hamming distance
    '''
    A = set(A)
    B = set(B)
    return len((A - B) | (B - A))

def levenshtein_distance(s, t):
    '''
    Dynamic programming-based calculation of Levenshtein distance between s and t
           Parameters:
                   s (str): Source string
                   t (str): Target string
           
           Returns:
                   (float): Levenshtein distance
    '''
    # From https://en.wikipedia.org/wiki/Levenshtein_distance#Iterative_with_full_matrix
    # TODO: How to handle None values for this and for Jaccard distance
    
    m = len(s)
    n = len(t)
    
    
    # For all i and j, d[i, j] will hold the Levenshtein distance between
    # the first i characters of s and the first j characters of t
    d = np.zeros(shape=(m+1, n+1))
    
    # Source prefixes can be transformed into empty string by
    # dropping all characters
    for i in range(m):
        d[i+1, 0] = i+1
    
    # Target prefixes can be reached from empty source prefix
    # by inserting every character
    for j in range(n):
        d[0, j+1] = j+1
    
    for j in range(n):
        for i in range(m):
            if s[i] == t[j]:
                substitution_cost = 0
            else:
                substitution_cost = 1
            d[i+1, j+1] = min(d[i, j+1] + 1,                  # Deletion
                              d[i+1, j] + 1,                  # Insertion
                              d[i, j] + substitution_cost     # Substitution
                             )
    return d[m, n]


def haversine_distance(longit_a, latit_a, longit_b, latit_b):
    '''
    Calculate Haversine distance between two points
           Parameters:
                   longit_a (float): Longitude of point A
                   latit_a (float): Latitude of point A
                   longit_b (float): Longitude of point B
                   latit_b (float): Latitude of point B
           Returns:
                   (float): Haversine distance
    '''
    if longit_a is None or latit_a is None:
        return None
    if longit_b is None or latit_b is None:
        return None
    
    # Transform to radians
    longit_a, latit_a, longit_b, latit_b = map(math.radians, [longit_a, latit_a, longit_b, latit_b])
    dist_longit = longit_b - longit_a
    dist_latit = latit_b - latit_a
    
    # Calculate area
    area = math.sin(dist_latit/2)**2 + math.cos(latit_a) * math.cos(latit_b) * math.sin(dist_longit/2)**2
    
    # Calculate the central angle
    central_angle = 2 * math.asin(math.sqrt(area))
    
    radius = 6371
    
    # Calculate Distance
    distance = central_angle * radius
    
    return abs(round(distance, 2))