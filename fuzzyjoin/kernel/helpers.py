#!/usr/bin/env python
# coding: utf-8

# Import Packages
## Standard Packages


## Custom Packages
import fuzzyjoin.filtering.reduction_filtering as rf

def pass_filters(A, B, token, token_ordering):
    '''
    Applies filters (length, positional) to record projection pair for reduce step of Basic Kernel
           Parameters:
                   A (list): Input string A, as list of tokens
                   B (list): Input string B, as list of tokens
                   token (str): input token
                   token_ordering (list): Global token ordering
           
           Returns:
                   (bool): True if pair satisfies all filters; False otherwise
    '''
    if not rf.jaccard_length_filtering(A, B):
        return False
    if not rf.jaccard_positional_filtering(A, B, token):
        return False
# TODO: Be sure to sort each list of tokens only by tokens available in global token ordering
#        A_ordered = sorted(A, key=lambda x: token_ordering.index(x))
#        B_ordered = sorted(B, key=lambda x: token_ordering.index(x))
#        if not jaccard_suffix_filter(A_ordered, B_ordered):
#            return False
    return True